# Proyecto Cafeteria (Sistema web para la recepcion de pedidos en una Cafeteria) - Bootcamp FPUNA

# Prueba del proyecto
Para hacer una prueba del proyecto podemos clonar el repositorio en la maquina local, para esto debemos tener git instalado en el sistema, para ejecutar el proyecto también debemos tener instalado python en el sistema, para verificar si tenemos instalado ejecutamos **python --version** en una terminal de comandos. Una vez que comprobamos que tenemos instalado python, nos ubicamos en la raiz del proyecto, y creamos un nuevo entorno virtual(opcional), lo activamos y luego instalamos los requerimientos del sistema. Posteriormente iniciamos el servidor con el comando **python manage.py runserver**, accedemos a dirección `http://localhost:8000/` (o cualquier otra dirección que indique la salida del comando de ejecución)


## Clonar repositorio
 
* git clone https://gitlab.com/bootcamp-fpuna9470318/proyecto-cafeteria-backend.git
*  cd proyecto-cafeteria-backend



## Creacion y activacion de entorno virtual en Windows

* python -m venv env
* env\Scripts\activate.bat

## Intalacion de los requerimientos 

* pip install -r requirements.txt

## Realizar migraciones:

Ejecuta las migraciones de Django para crear las tablas y estructuras necesarias en la base de datos MongoDB. En la línea de comandos, ejecuta el siguiente comando:

* python manage.py migrate

Esto aplicará todas las migraciones pendientes en la base de datos MongoDB.

# Ejecutar el servidor de desarrollo

- python manage.py runserver

- Iniciará el servidor de desarrollo y escuchará en la dirección http://127.0.0.1:8000/.

Ahi deberiamos ver el proyecto backend en funcionamiento

# Ejecutar el frontend

Navega hasta la carpeta "CafeteriaFrontend" dentro del proyecto Django clonado utilizando el siguiente comando en la terminal:

- cd <ruta_del_proyecto>/CafeteriaFrontend

Reemplaza <ruta_del_proyecto> con la ruta absoluta a la carpeta raíz del proyecto Django.

- Asegúrate de tener Node.js y npm instalados en tu sistema.

Ejecuta el siguiente comando para instalar las dependencias del proyecto Vite:

- npm install

Después de que las dependencias se hayan instalado correctamente, puedes ejecutar el proyecto React utilizando el siguiente comando:

- npm run dev

Esto iniciará el servidor de desarrollo de Vite y escuchará en la dirección http://localhost:3000.

- Abrimos la dirección http://localhost:3000/

Ahi deberiamos ver el frontend en funcionamiento

# Logueo en el sistema

El sistema cuenta con un usuarios predefinido, un usuario recepcionista que seria un Asesor de atención. 

**Usuario Asesor**
- Usuario: recepcionista
- Contraseña: recepcionista
