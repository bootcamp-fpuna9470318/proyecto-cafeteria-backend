from django.db import models

# Create your models here.

class Pedido(models.Model):
    mesa = models.IntegerField(default=0)
    descripcion = models.CharField(max_length=100, default=None)
    fecha_inicio = models.DateTimeField(null=True)
    fecha_fin = models.DateTimeField(null=True)
    productos = models.JSONField(default=list)
    total = models.IntegerField(default=0)  

    estado = models.IntegerField(default=0)  
    cliente = models.CharField(max_length=100, default=None)

    class Meta:
        verbose_name = "Pedido"
        verbose_name_plural = "Pedidos"
        ordering = ['id']

    def __str__(self):
        return self.descripcion