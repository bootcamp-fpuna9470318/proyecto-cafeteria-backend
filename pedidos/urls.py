from django.urls import path, include
from rest_framework import routers
from pedidos import views


router = routers.DefaultRouter()
router.register(r'pedidos', views.PedidoView)


urlpatterns = [
    path('', include(router.urls))
]
