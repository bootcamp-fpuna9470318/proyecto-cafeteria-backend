from django.db import models

# Create your models here.

class Usuario(models.Model):
    usuario = models.CharField(max_length=100, default=None)
    nombre = models.CharField(max_length=100, default=None)
    apellido = models.CharField(max_length=100, default=None)
    tipo_usuario = models.CharField(max_length=50, default=None)
    password = models.CharField(max_length=50,default="")
    email =  models.EmailField(max_length=50,default="")
    telefono = models.CharField(max_length=50, default="")

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"
        ordering = ['id']

    def __str__(self):
        return self.nombre + " "+ self.apellido