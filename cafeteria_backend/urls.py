from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect

def redirect_to_usuarios(request):
    return redirect('usuarios/')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', redirect_to_usuarios),  # Redireccionar la página principal a usuarios/
    path('',include('pedidos.urls')),
    path('',include('productos.urls')),
    path('',include('usuarios.urls')),
]
