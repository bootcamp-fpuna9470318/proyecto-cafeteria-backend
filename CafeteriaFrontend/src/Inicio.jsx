import React from 'react';
import { useHistory } from 'react-router-dom';
import './Inicio.css';

function Inicio() {

  const usuarioId = localStorage.getItem('usuarioId');


  const history = useHistory();

  const handleNewPedido = () => {
    history.push('/new-pedido');
  };

  const handlePedidos = () => {
    history.push('/pedidos');
  };

  const handleMiPerfil = () => {
    history.push('/mi-perfil');
  };

  const handleSalir = () => {
    // Realiza la lógica para cerrar sesión aquí
    // Por ejemplo, borrar el token de autenticación

    // Redirige a la página de inicio de sesión después de cerrar sesión
    history.push('/');
  };

  return (
    <div className="inicio-page">
      <nav className="navbar">
        <div className="message-container-inicio">
          <h1 className="message-title">Your coffee favorite . . .</h1>
        </div>
      </nav>
      <h1 className="logo-title">Coffee</h1>
      <h4 className="descripcion-pag-inicio">----------------------------------------------------</h4>
      <button className="grupo-button" onClick={handleNewPedido}>
        NEW PEDIDO
      </button>
      <br />
      <button className="grupo-button" onClick={handlePedidos}>
        PEDIDOS
      </button>
      <br />
      <button className="grupo-button" onClick={handleMiPerfil}>
        MI PERFIL
      </button>
      <br />
      <button className="grupo-button" onClick={handleSalir}>
        SALIR
      </button>
      <br />
      <h4 className="descripcion-pag">----------------------------------------------------</h4>
      <>Bootcamp FPUNA - Año 2023</>
    </div>
  );
}

export default Inicio;
