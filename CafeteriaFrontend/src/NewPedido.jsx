import React, { useState } from 'react';
import './NewPedido.css';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

function NewPedido() {
  const history = useHistory();
  const [cliente, setCliente] = useState('');

  const handleAtras = () => {
    history.goBack();
  };

  const handleInicio = () => {
    history.push('/inicio');
  };

  const handleMiPerfil = () => {
    history.push('/mi-perfil');
  };

  const handleSalir = () => {
    // Realiza la lógica para cerrar sesión aquí
    // Por ejemplo, borrar el token de autenticación

    // Redirige a la página de inicio de sesión después de cerrar sesión
    history.push('/');
  };

  const handleSiguiente = async () => {
    try {
      // Realizar el POST para crear el pedido con el nombre del cliente
      const response = await axios.post('http://127.0.0.1:8000/pedidos/', {
        cliente: cliente,
      });
  
      // Obtener el ID del pedido de la respuesta del servidor
      const pedidoId = response.data.id;
  
      // Redirigir al usuario a la página ElegirProductos con el ID del pedido en la ruta
      history.push(`/elegir-productos/${pedidoId}`);
    } catch (error) {
      console.error('Error al crear el pedido:', error);
    }
  };
  

  const handleClienteChange = (event) => {
    setCliente(event.target.value);
  };

  return (
    <div className="login-page">
      <nav className="navbar">
        <div className="message-container" style={{ display: "flex" }}>
          <h1 className="message-title"> Coffee </h1>
        </div>
        <div style={{ float: "right", display: "flex", gridGap: "50px", marginRight: "60px" }}>
          <h1 className="opciones-nav" onClick={handleAtras}> Atras </h1>
          <h1 className="opciones-nav" onClick={handleInicio}> Inicio </h1>
          <h1 className="opciones-nav" onClick={handleMiPerfil}> Perfil </h1>
          <button className="salir-button" onClick={handleSalir}> Cerrar Sesion </button>
        </div>
      </nav>
      <h1 className="newPedido-title">Nuevo Pedido</h1>
      <h4 className='descripcion-pag'>Introduce el nombre del cliente de Coffee <br />o la dirección de correo electrónico</h4>
      <div className="form-group">
        <label
          htmlFor="nombre-cliente"
          style={{ fontWeight: "bold", verticalAlign: "top" }}
        >
          Nombre del cliente <br />
        </label>
        <br />
        <input
          type="text"
          id="nombre-cliente"
          className="input-user-field"
          value={cliente}
          onChange={handleClienteChange}
        />
      </div>
      <button className="siguiente-button" onClick={handleSiguiente}>
        SIGUIENTE
      </button>
    </div>
  );
}

export default NewPedido;
