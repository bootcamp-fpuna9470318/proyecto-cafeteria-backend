import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import Login from './Login';
import Inicio from './Inicio';
import NewPedido from './NewPedido';
import ElegirProductos from './ElegirProductos';
import Carrito from './Carrito';
import Pedidos from './Pedidos';
import Perfil from './Perfil';
import Prueba from './Prueba';

function App() {
  return (
    <Router>
      <div className="app-container">
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/inicio" component={Inicio} />
          <Route path="/new-pedido" component={NewPedido} />
          <Route path="/elegir-productos/:pedidoId" component={ElegirProductos} />
          <Route path="/carrito/:pedidoId" component={Carrito} />
          <Route path="/pedidos" component={Pedidos} />
          <Route path="/mi-perfil" component={Perfil} />
          <Route path="/prueba" component={Prueba} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
