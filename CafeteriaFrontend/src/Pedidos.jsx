import React, { useState, useEffect } from "react";
import "./Pedidos.css";
import { useHistory } from 'react-router-dom';
import axios from 'axios';

function Pedidos() {
  const history = useHistory();
  const [pedidos, setPedidos] = useState([]);

  useEffect(() => {
    axios.get('http://127.0.0.1:8000/pedidos/')
      .then(response => {
        setPedidos(response.data);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  const handleAtras = () => {
    history.goBack();
  };

  const handleInicio = () => {
    history.push('/inicio');
  };

  const handleMiPerfil = () => {
    history.push('/mi-perfil');
  };

  const handleSalir = () => {
    // Realiza la lógica para cerrar sesión aquí
    // Por ejemplo, borrar el token de autenticación

    // Redirige a la página de inicio de sesión después de cerrar sesión
    history.push('/');
  };

  const handleCancelarPedido = (pedidoId) => {
    const confirmarCancelar = window.confirm("¿Estás seguro de cancelar este pedido?");
    if (confirmarCancelar) {
      axios.delete(`http://127.0.0.1:8000/pedidos/${pedidoId}`)
        .then(response => {
          // Eliminar el pedido de la lista
          setPedidos(pedidos.filter(pedido => pedido.id !== pedidoId));
        })
        .catch(error => {
          console.error("Error al cancelar el pedido:", error);
        });
    }
  };

  const handleMarcarEntregado = (pedidoId) => {
    const confirmarEntregado = window.confirm("¿Estás seguro de marcar este pedido como entregado?");
    if (confirmarEntregado) {
      axios.put(`http://127.0.0.1:8000/pedidos/${pedidoId}/`, { estado: 1 })
        .then(response => {
          // Actualizar el estado del pedido a 1
          const pedidosActualizados = pedidos.map(pedido => {
            if (pedido.id === pedidoId) {
              return { ...pedido, estado: 1 };
            }
            return pedido;
          });
          setPedidos(pedidosActualizados);
        })
        .catch(error => {
          console.error("Error al marcar el pedido como entregado:", error);
        });
    }
  };

  return (
    <div>
      <nav className="navbar">
        <div className="message-container-inicio" style={{ display: "flex" }}>
          <h1 className="message-title"> Listado de Pedidos </h1>
        </div>
        <div style={{ float: "right", display: "flex", gridGap: "50px", marginRight: "60px" }}>
          <h1 className="opciones-nav" onClick={handleAtras}> Atras </h1>
          <h1 className="opciones-nav" onClick={handleInicio}> Inicio </h1>
          <h1 className="opciones-nav" onClick={handleMiPerfil}> Perfil </h1>
          <button className="salir-button" onClick={handleSalir}> Cerrar Sesion </button>
        </div>
      </nav>
      <div style={{ display: "flex", alignItems: "center", gridGap: "520px" }}>
        <h2>En curso . . .</h2>
        <h2>Completado</h2>
      </div>

      <div className="tarjeta-container">
        <div className="cuadro-izquierdo">
          {/* Contenido del cuadro izquierdo */}
          {pedidos
            .filter(pedido => pedido.estado === 0)
            .map(pedido => (
              <div
                className="tarjetita"
                key={pedido.id}
                style={{ display: "flex", gridGap: "70px" }}
              >
                <h3><br /> Nro.   {pedido.id}</h3>
                <div>
                  <h4>Cliente: {pedido.cliente}</h4>
                  <p style={{ color: "purple" }}><b>Preparando . . .</b> </p>
                  <p>Total: {pedido.total} Gs</p>
                </div>
                <div style={{ marginLeft: "auto" }}>
                  <br /><br />
                  <button style={{ left: "10px" }} onClick={() => handleCancelarPedido(pedido.id)}>Cancelar</button>
                  <button style={{ marginLeft: "10px" }} onClick={() => handleMarcarEntregado(pedido.id)}>✓</button>
                </div>
              </div>
            ))}
        </div>
        <div className="cuadro-derecho">
          {/* Contenido del cuadro derecho */}
          {pedidos
            .filter(pedido => pedido.estado === 1)
            .map(pedido => (
              <div
                className="tarjetita"
                key={pedido.id}
                style={{ display: "flex", gridGap: "120px" }}
              >
                <h3><br />Nro. {pedido.id}</h3>
                <div>
                  <h4>Cliente: {pedido.cliente}</h4>
                  <p style={{ color: "green" }}><b>Entregado</b> </p>
                  <p>Total: {pedido.total} Gs</p>
                </div>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
}

export default Pedidos;
