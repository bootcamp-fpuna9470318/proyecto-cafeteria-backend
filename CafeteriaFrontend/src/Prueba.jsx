import React, { useEffect } from 'react';
import axios from 'axios';

const Prueba = () => {
  useEffect(() => {
    axios.get('http://127.0.0.1:8000/usuarios/')
      .then(response => {
        // Aquí puedes manejar la respuesta de la API
        console.log(response.data);
      })
      .catch(error => {
        // Aquí puedes manejar los errores
        console.error(error);
      });
  }, []);

  return <div>Contenido de tu componente JSX</div>;
};

export default Prueba;
