import React, { useState } from 'react';
import axios from 'axios';
import './ElegirProductos.css';
import { useHistory, useParams } from 'react-router-dom';

const ElegirProductos = () => {
  const history = useHistory();
  const { pedidoId } = useParams();

  const handleAtras = () => {
    history.goBack();
  };

  const handleInicio = () => {
    history.push('/inicio');
  };

  const handleMiPerfil = () => {
    history.push('/mi-perfil');
  };

  const handleSalir = () => {
    // Realiza la lógica para cerrar sesión aquí
    // Por ejemplo, borrar el token de autenticación

    // Redirige a la página de inicio de sesión después de cerrar sesión
    history.push('/');
  };

  const handleCarrito = () => {
    actualizarPedido(); 
    history.push(`/carrito/${pedidoId}`);
  };

  const [productosAgregados, setProductosAgregados] = useState([]);
  const [montoTotal, setMontoTotal] = useState(0);

  const handleAgregarProducto = (producto) => {
    const productoExistente = productosAgregados.find((p) => p.nombre === producto.nombre);

    if (productoExistente) {
      // Si el producto ya está en la lista, actualiza la cantidad
      const nuevosProductos = productosAgregados.map((p) =>
        p.nombre === producto.nombre ? { ...p, cantidad: p.cantidad + 1 } : p
      );
      setProductosAgregados(nuevosProductos);
    } else {
      // Si el producto no está en la lista, agrégalo
      setProductosAgregados([...productosAgregados, { ...producto, cantidad: 1 }]);
    }

    // Actualiza el monto total
    setMontoTotal(montoTotal + producto.precio);
  };

  

  const actualizarPedido = async () => {
    try {
      await axios.put(`http://127.0.0.1:8000/pedidos/${pedidoId}/`, {
        productos: productosAgregados,
        total: montoTotal,
      });
      console.log('Pedido actualizado exitosamente');
    } catch (error) {
      console.error('Error al actualizar el pedido:', error);
    }
  };  

  return (
    <div className="login-page">
      <div>
        <nav className="navbar">
          <div className="message-container-inicio" style={{ display: 'flex' }}>
            <h1 className="message-title"> Listado de Productos </h1>
          </div>
          <div
            style={{
              float: 'right',
              display: 'flex',
              gridGap: '50px',
              marginRight: '60px',
            }}
          >
            <h1 className="opciones-nav" onClick={handleAtras}>
              {' '}
              Atras{' '}
            </h1>
            <h1 className="opciones-nav" onClick={handleInicio}>
              {' '}
              Inicio{' '}
            </h1>
            <h1 className="opciones-nav" onClick={handleMiPerfil}>
              {' '}
              Perfil{' '}
            </h1>
            <button className="salir-button" onClick={handleSalir}>
              {' '}
              Cerrar Sesion{' '}
            </button>
          </div>
        </nav>
      </div>
      <br></br>
      <br></br>
      <div style={{ display: 'flex', gridGap: '50px' }}>
        {/* Tarjeta de producto 1 */}
        <div className="product-card">
          <img
            src="/images/cafe-americano.jpg"
            alt="Producto"
            className="product-image"
          />
          <h2 className="product-title">Café Americano</h2>
          <p className="product-price">Gs. 15.000</p>
          <p className="product-description">
            Un café clásico y suave preparado con espresso y agua caliente.
          </p>
          <button className="add-to-cart-button" onClick={() => handleAgregarProducto({ nombre: 'Café Americano', precio: 15000 })}>
            Agregar
          </button>
        </div>

        {/* Tarjeta de producto 2 */}
        <div className="product-card">
          <img
            src="/images/cafe-con-leche.jpg"
            alt="Producto"
            className="product-image"
          />
          <h2 className="product-title">Café con Leche</h2>
          <p className="product-price">Gs. 10.000</p>
          <p className="product-description">
            Una mezcla equilibrada de café y leche caliente, para disfrutar de
            un sabor suave y cremoso.
          </p>
          <button className="add-to-cart-button" onClick={() => handleAgregarProducto({ nombre: 'Café con Leche', precio: 10000 })}>
            Agregar
          </button>
        </div>

        {/* Tarjeta de producto 3 */}
        <div className="product-card">
          <img
            src="/images/mixto.jpg"
            alt="Producto"
            className="product-image"
          />
          <h2 className="product-title">Mixto</h2>
          <p className="product-price">Gs. 12.000</p>
          <p className="product-description">
            Un delicioso sándwich con jamón y queso derretido, la combinación
            perfecta entre lo salado y lo cremoso.
          </p>
          <button className="add-to-cart-button" onClick={() => handleAgregarProducto({ nombre: 'Mixto', precio: 12000 })}>
            Agregar
          </button>
        </div>

        {/* Tarjeta de producto 4 */}
        <div className="product-card">
          <img
            src="/images/jugo-de-frutilla.jpg"
            alt="Producto"
            className="product-image"
          />
          <h2 className="product-title">Jugo de Frutilla</h2>
          <p className="product-price">Gs. 17.000</p>
          <p className="product-description">
            Un refrescante jugo hecho con jugo de frutilla fresca, para
            disfrutar de un sabor dulce y afrutado.
          </p>
          <button className="add-to-cart-button" onClick={() => handleAgregarProducto({ nombre: 'Jugo de Frutilla', precio: 17000 })}>
            Agregar
          </button>
        </div>
      </div>

      <br></br>
      <div style={{ display: 'flex', gridGap: '50px' }}>
        {/* Tarjeta de producto 5 */}
        <div className="product-card">
          <img
            src="/images/agua-500-ml.jpg"
            alt="Producto"
            className="product-image"
          />
          <h2 className="product-title">Agua 500 ml</h2>
          <p className="product-price">Gs. 5.000</p>
          <p className="product-description">
            Una botella de agua mineral pura y refrescante, ideal para
            hidratarse.
          </p>
          <button className="add-to-cart-button" onClick={() => handleAgregarProducto({ nombre: 'Agua 500 ml', precio: 5000 })}>
            Agregar
          </button>
        </div>

        {/* Tarjeta de producto 6 */}
        <div className="product-card">
          <img
            src="/images/bebida-gaseosa-de-500-ml.jpg"
            alt="Producto"
            className="product-image"
          />
          <h2 className="product-title">Bebida Gaseosa de 500 ml</h2>
          <p className="product-price">Gs. 5.000</p>
          <p className="product-description">
            Una bebida gaseosa carbonatada en un vaso de 500 ml, para disfrutar
            de un sabor burbujeante y refrescante.
          </p>
          <button className="add-to-cart-button" onClick={() => handleAgregarProducto({ nombre: 'Bebida Gaseosa de 500 ml', precio: 5000 })}>
            Agregar
          </button>
        </div>
      </div>

      <button className="floating-button" onClick={handleCarrito}>
        Ir al carrito . . . 🛒
      </button>
      <h1 className="floating-button-2">
        Monto total a pagar: Gs. {montoTotal}
      </h1>
    </div>
    
  );
};

export default ElegirProductos;
