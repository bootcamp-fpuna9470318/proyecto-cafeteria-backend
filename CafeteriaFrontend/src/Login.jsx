import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import './Login.css';

function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const history = useHistory();
  const [listaUsuarios, setUsuarios] = useState([]);

  useEffect(() => {
    axios.get('http://127.0.0.1:8000/usuarios/')
      .then(response => {
        const data = response.data;
  
        if (Array.isArray(data) && data.length === 0) {
          // La lista de usuarios está vacía
          // Realizar la solicitud POST para crear un nuevo usuario
          const newUser = {
            usuario: 'recepcionista',
            nombre: 'Juan',
            apellido: 'Perez',
            tipo_usuario: 'recepcionista',
            password: 'recepcionista',
            email: 'juan@gmail.com',
            telefono: '985 984 984',
          };
        
          axios.post('http://127.0.0.1:8000/usuarios/', newUser)
            .then(response => {
              // Realizar alguna acción después de la creación exitosa del usuario
              console.log(response.data);
            })
            .catch(error => {
              console.error(error);
            });
        }
        
        setUsuarios(data);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);
  

  const handleLogin = () => {
    axios.get('http://127.0.0.1:8000/usuarios/')
      .then(response => {
        const user = response.data.find(user => user.usuario === email);
        // Obtener el ID del pedido de la respuesta del servidor
        console.log(response.data)
        if (user) {
          if (user.password === password) {
            history.push('/inicio');
          } else {
            setErrorMessage('Contraseña incorrecta');
          }
          const usuarioId = user.id;

          localStorage.setItem('usuarioId', user.id);
        } else {
          setErrorMessage('El correo electrónico o usuario no existe');
        }

        

      })
      .catch(error => {
        console.error(error);
      });
  };

  return (
    <div className="login-page">
      <nav className="navbar">
        <h1 className="massage-title-login">Inicie sesión para continuar</h1>
      </nav>
      <h1 className="coffee-title">Coffee</h1>
      <div className="form-group">
        <label htmlFor="email" style={{ fontWeight: 'bold', verticalAlign: 'top' }}>
          Correo Electrónico o Usuario
        </label>
        <br />
        <input
          type="text"
          id="email"
          className="input-field"
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="password" style={{ fontWeight: 'bold', verticalAlign: 'top' }}>
          Contraseña
        </label>
        <br />
        <input
          type="password"
          id="password"
          className="input-field"
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
      </div>
      <button className="login-button" onClick={handleLogin}>
        INICIAR SESIÓN
      </button>
      <p className="error-message">{errorMessage}</p>
    </div>
  );
}

export default Login;
