import React, { useState, useEffect } from "react";
import axios from "axios";
import "./Login.css";
import "./Perfil.css";
import { useHistory } from "react-router-dom";

const Perfil = () => {
  const usuarioId = localStorage.getItem('usuarioId');

  const [usuario, setUsuario] = useState(null);
  const history = useHistory();

  const handleAtras = () => {
    history.goBack();
  };

  const handleInicio = () => {
    history.push("/inicio");
  };

  const handleSalir = () => {
    // Realiza la lógica para cerrar sesión aquí
    // Por ejemplo, borrar el token de autenticación

    // Redirige a la página de inicio de sesión después de cerrar sesión
    history.push("/");
  };

  const handleCarrito = () => {
    history.push("/carrito");
  };

  useEffect(() => {
    const fetchUsuario = async () => {
      try {
        const response = await axios.get(`http://127.0.0.1:8000/usuarios/${usuarioId}`);
        const data = response.data;
        setUsuario(data);
      } catch (error) {
        console.error("Error al obtener el usuario:", error);
      }
    };

    fetchUsuario();
  }, [usuarioId]);

  return (
    <div className="login-page">
      <div>
        <nav className="navbar">
          <div className="message-container-inicio" style={{ display: "flex" }}>
            <h1 className="message-title"> Mi Perfil </h1>
          </div>
          <div
            style={{
              float: "right",
              display: "flex",
              gridGap: "50px",
              marginRight: "60px",
            }}
          >
            <h1 className="opciones-nav" onClick={handleAtras}>
              {" "}
              Atras{" "}
            </h1>
            <h1 className="opciones-nav" onClick={handleInicio}>
              {" "}
              Inicio{" "}
            </h1>
            <button className="salir-button" onClick={handleSalir}>
              {" "}
              Cerrar Sesion{" "}
            </button>
          </div>
        </nav>
      </div>
      <br></br>
      <br></br>
      <div style={{ display: "flex", gridGap: "50px" }}>
        <div className="product-card-perfil">
          <img
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH4skylfJs-mOf6lz4pGDuTMvX6zqPc4LppQ&usqp=CAU"
            alt="Producto"
            className="product-image-perfil"
          />
          {usuario && (
            <>
              <h2 className="product-title-perfil">{usuario.nombre} {usuario.apellido}</h2>
              <p>{usuario.tipo_usuario}</p>
              <p className="product-price-perfil">{usuario.email}</p>
              <p className="product-description-perfil">+595 {usuario.telefono}</p>
              <br></br>
              <button className="add-to-cart-button-perfil">Editar</button>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Perfil;
