import React, { useState, useEffect } from "react";
import axios from "axios";
import "./Carrito.css";
import "./ButtonContainer.css";
import { useHistory, useParams } from "react-router-dom";

const Carrito = () => {
  
  const history = useHistory();
  const { pedidoId } = useParams();

  const [pedido, setPedido] = useState(null);
  const [montoTotal, setMontoTotal] = useState(0);
  const [productosCarrito, setProductosCarrito] = useState([]);

  const handleConfirmar = () => {
    const productosActualizados = [...productosCarrito];
    const pedidoActualizado = { ...pedido, productos: productosActualizados };
    axios
      .put(`http://127.0.0.1:8000/pedidos/${pedidoId}/`, { productos: productosActualizados, total: montoTotal })
      .then(response => {
        console.log("Pedido actualizado:", response.data);
        history.push(`/pedidos/`);
      })
      .catch(error => {
        console.error("Error al actualizar el pedido:", error);
      });
  };
  

  const removeAccents = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  };

  useEffect(() => {
    const fetchPedido = async () => {
      try {
        const response = await axios.get(
          `http://127.0.0.1:8000/pedidos/${pedidoId}`
        );
        const data = response.data;
        setPedido(data);
        setProductosCarrito(data.productos);
        calcularMontoTotal(data.productos);
      } catch (error) {
        console.error("Error al obtener el pedido:", error);
      }
    };

    fetchPedido();
  }, [pedidoId]);

  const handleAtras = () => {
    history.goBack();
  };

  const handleInicio = () => {
    history.push("/inicio");
  };

  const handleMiPerfil = () => {
    history.push("/mi-perfil");
  };

  const handleSalir = () => {
    history.push("/");
  };

  const calcularMontoTotal = (productos) => {
    let total = 0;
    productos.forEach((producto) => {
      total += producto.precio * producto.cantidad;
    });
    setMontoTotal(total);
  };

  

  const handleQuitar = async (index) => {
    const nuevosProductos = [...productosCarrito];
    const producto = nuevosProductos[index];
    if (producto.cantidad > 1) {
      producto.cantidad -= 1;
      setProductosCarrito(nuevosProductos);
      calcularMontoTotal(nuevosProductos);
    } else {
      try {
        await axios.delete(`http://127.0.0.1:8000/pedidos/${pedidoId}/productos/${producto.id}`);
        nuevosProductos.splice(index, 1); // Eliminar el producto del array
        setProductosCarrito(nuevosProductos);
        calcularMontoTotal(nuevosProductos);
      } catch (error) {
        console.error("Error al eliminar el producto:", error);
      }
    }
  };
  

  const handleAgg = (index) => {
    const nuevosProductos = [...productosCarrito];
    const producto = nuevosProductos[index];
    producto.cantidad += 1;
    setProductosCarrito(nuevosProductos);
    calcularMontoTotal(nuevosProductos);
  };

  if (!pedido) {
    return <div>Cargando pedido...</div>;
  }

  return (
    <div className="login-page">
      <div>
        <nav className="navbar">
          <div className="message-container-inicio" style={{ display: "flex" }}>
            <h1 className="message-title"> Carrito </h1>
          </div>
          <div
            style={{
              float: "right",
              display: "flex",
              gridGap: "50px",
              marginRight: "60px",
            }}
          >
            <h1 className="opciones-nav" onClick={handleAtras}>
              Atras
            </h1>
            <h1 className="opciones-nav" onClick={handleInicio}>
              Inicio
            </h1>
            <h1 className="opciones-nav" onClick={handleMiPerfil}>
              Perfil
            </h1>
            <button className="salir-button" onClick={handleSalir}>
              Cerrar Sesión
            </button>
          </div>
        </nav>
      </div>
      <br />
      <br />
      <div>
        {productosCarrito.map((producto, index) => (
          <div className="product-card-carrito" key={index}>
            <div className="product-card-content">
              <div style={{ display: "flex", gridGap: "15px" }}>
                <img
                  src={`/images/${removeAccents(producto.nombre)
                    .toLowerCase()
                    .replace(/[^\w\s]/gi, "")
                    .replace(/\s+/g, "-")
                    .replace(/-{2,}/g, "-")
                    .replace(/\//g, "-") // Reemplazar "/" por "-"
                    .trim()}.jpg`}
                  alt="Producto"
                  className="product-image"
                />
                <div>
                  <br />
                  <h2 className="product-title">{producto.nombre}</h2>
                  <p className="product-price">Gs. {producto.precio}</p>
                </div>
              </div>
              <div style={{ display: "flex", gridGap: "15px" }}>
                <div className="info-final">
                  <br />
                  <p>Cantidad: {producto.cantidad}</p>
                  <p className="product-price">
                    Gs. {producto.precio} x {producto.cantidad} unid.
                  </p>
                  <p>Total: Gs {producto.precio * producto.cantidad}</p>
                </div>
                <div className="button-container">
                  <button className="delete-button" onClick={() => handleQuitar(index)}>
                    Quitar
                  </button>
                  <button className="add-button" onClick={() => handleAgg(index)}>
                    Agg
                  </button>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
      <button className="floating-button" onClick={handleConfirmar}>
        Confirmar ✔️
      </button>
      <h1 className="floating-button-2">
        Monto total a pagar: Gs. {montoTotal}
      </h1>
    </div>
  );
};

export default Carrito;
